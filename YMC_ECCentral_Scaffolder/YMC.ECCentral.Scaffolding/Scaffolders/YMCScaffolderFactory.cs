﻿using EnvDTE;
using Microsoft.AspNet.Scaffolding;
using Microsoft.AspNet.Scaffolding.NuGet;
using Microsoft.AspNet.Scaffolding.EntityFramework.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using YMC.ECCentral.Scaffolding.UI;
using System.Windows.Media;
using System.Drawing;
using System.Windows.Interop;
using System.Windows;
using System.Windows.Media.Imaging;
using YMC.ECCentral.Scaffolding.Scaffolders;

namespace YMC.ECCentral.Scaffolding
{    

    //// This is where everything with the scaffolder is kicked off. The factory
    //// returns a MvcScaffolderFactory when a project meets the requirements.
    //[Export(typeof(CodeGeneratorFactory))]
    //public class FamiScaffolderFactory : CodeGeneratorFactory
    //{
    //    public FamiScaffolderFactory()
    //        : base(CreateCodeGeneratorInformation())
    //    {

    //    }

    //    public override ICodeGenerator CreateInstance(CodeGenerationContext context)
    //    {
    //        return new MvcScaffolder(context, Information);
    //    }
      
    //    // We support CSharp WAPs targetting at least .Net Framework 4.5 or above.
    //    // We DON'T currently support VB
    //    public override bool IsSupported(CodeGenerationContext codeGenerationContext)
    //    {
    //        if (ProjectLanguage.CSharp.Equals(codeGenerationContext.ActiveProject.GetCodeLanguage()) )
    //        {
    //            FrameworkName targetFramework = codeGenerationContext.ActiveProject.GetTargetFramework();
    //            return (targetFramework != null) &&
    //                    String.Equals(".NetFramework", targetFramework.Identifier, StringComparison.OrdinalIgnoreCase) &&
    //                    targetFramework.Version >= new Version(4, 5);
    //        }

    //        return false;
    //    }

    //    private static CodeGeneratorInformation CreateCodeGeneratorInformation()
    //    {
    //        return new CodeGeneratorInformation(
    //            displayName: Resources.MVCScaffolder_Name,
    //            description: Resources.MVCScaffolder_Description,
    //            author: "阳铭",
    //            version: new Version(0, 1, 5, 27),
    //            id: typeof(MvcScaffolder).Name,
    //            icon: ToImageSource(Resources.Application),
    //            gestures: new[] { "Controller" },
    //            categories: new[] { Categories.Common, Categories.MvcController, Categories.Other }
    //        );              
    //    }

    //    /// <summary>
    //    /// Helper method to convert Icon to Imagesource.
    //    /// </summary>
    //    /// <param name="icon">Icon</param>
    //    /// <returns>Imagesource</returns>
    //    public static ImageSource ToImageSource(Icon icon)
    //    {
    //        ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
    //            icon.Handle,
    //            Int32Rect.Empty,
    //            BitmapSizeOptions.FromEmptyOptions());

    //        return imageSource;
    //    }
    //}


    //[Export(typeof(CodeGeneratorFactory))]
    //public class MvcScaffolderFactory2 : CodeGeneratorFactory
    //{
    //    public MvcScaffolderFactory2()
    //        : base(CreateCodeGeneratorInformation())
    //    {

    //    }

    //    public override ICodeGenerator CreateInstance(CodeGenerationContext context)
    //    {
    //        return new MvcScaffolderSP(context, Information);
    //    }

    //    // We support CSharp WAPs targetting at least .Net Framework 4.5 or above.
    //    // We DON'T currently support VB
    //    public override bool IsSupported(CodeGenerationContext codeGenerationContext)
    //    {
    //        if (ProjectLanguage.CSharp.Equals(codeGenerationContext.ActiveProject.GetCodeLanguage()))
    //        {
    //            FrameworkName targetFramework = codeGenerationContext.ActiveProject.GetTargetFramework();
    //            return (targetFramework != null) &&
    //                    String.Equals(".NetFramework", targetFramework.Identifier, StringComparison.OrdinalIgnoreCase) &&
    //                    targetFramework.Version >= new Version(4, 5);
    //        }

    //        return false;
    //    }

    //    private static CodeGeneratorInformation CreateCodeGeneratorInformation()
    //    {
    //        return new CodeGeneratorInformation(
    //            displayName: Resources.MVCScaffolder_Name2,
    //            description: Resources.MVCScaffolder_Description2,
    //            author: "阳铭",
    //            version: new Version(0, 1, 5, 0),
    //            id: typeof(MvcScaffolder).Name,
    //            icon: ToImageSource(Resources.Application),
    //            gestures: new[] { "Controller" },
    //            categories: new[] { Categories.Common, Categories.MvcController, Categories.Other }
    //        );
    //    }

    //    /// <summary>
    //    /// Helper method to convert Icon to Imagesource.
    //    /// </summary>
    //    /// <param name="icon">Icon</param>
    //    /// <returns>Imagesource</returns>
    //    public static ImageSource ToImageSource(Icon icon)
    //    {
    //        ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
    //            icon.Handle,
    //            Int32Rect.Empty,
    //            BitmapSizeOptions.FromEmptyOptions());

    //        return imageSource;
    //    }
    //}

    [Export(typeof(CodeGeneratorFactory))]
    public class ModuleScaffolderFactory : CodeGeneratorFactory
    {
        public ModuleScaffolderFactory()
            : base(CreateCodeGeneratorInformation())
        {

        }

        public override ICodeGenerator CreateInstance(CodeGenerationContext context)
        {
            return new ModuleScaffolder(context, Information);
        }

        // We support CSharp WAPs targetting at least .Net Framework 4.5 or above.
        // We DON'T currently support VB
        public override bool IsSupported(CodeGenerationContext codeGenerationContext)
        {
            if (ProjectLanguage.CSharp.Equals(codeGenerationContext.ActiveProject.GetCodeLanguage()))
            {
                FrameworkName targetFramework = codeGenerationContext.ActiveProject.GetTargetFramework();
                return (targetFramework != null) &&
                        String.Equals(".NetFramework", targetFramework.Identifier, StringComparison.OrdinalIgnoreCase) &&
                        targetFramework.Version >= new Version(4, 5);
            }

            return false;
        }

        private static CodeGeneratorInformation CreateCodeGeneratorInformation()
        {
            return new CodeGeneratorInformation(
                displayName: "[ECCentral]添加模块功能",
                description: "通过实体类，生成相应模块的Application和Repository代码",
                author: "real罗阳铭",
                version: new Version(0, 1, 0, 0),
                id: "YMC_ECCentral_Scaffolding",
                icon: ToImageSource(Resources.Application),
                gestures: new[] { "YMC" },
                categories: new[] { "YMC", Categories.Common, Categories.Other }
            );
        }

        /// <summary>
        /// Helper method to convert Icon to Imagesource.
        /// </summary>
        /// <param name="icon">Icon</param>
        /// <returns>Imagesource</returns>
        public static ImageSource ToImageSource(Icon icon)
        {
            ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            return imageSource;
        }
    }

    [Export(typeof(CodeGeneratorFactory))]
    public class AreaScaffolderFactory : CodeGeneratorFactory
    {
        public AreaScaffolderFactory()
            : base(CreateCodeGeneratorInformation())
        {

        }

        public override ICodeGenerator CreateInstance(CodeGenerationContext context)
        {
            return new AreaScaffolder(context, Information);
        }

        // We support CSharp WAPs targetting at least .Net Framework 4.5 or above.
        // We DON'T currently support VB
        public override bool IsSupported(CodeGenerationContext codeGenerationContext)
        {
            if (ProjectLanguage.CSharp.Equals(codeGenerationContext.ActiveProject.GetCodeLanguage()))
            {
                FrameworkName targetFramework = codeGenerationContext.ActiveProject.GetTargetFramework();
                return (targetFramework != null) &&
                        String.Equals(".NetFramework", targetFramework.Identifier, StringComparison.OrdinalIgnoreCase) &&
                        targetFramework.Version >= new Version(4, 5);
            }

            return false;
        }

        private static CodeGeneratorInformation CreateCodeGeneratorInformation()
        {
            return new CodeGeneratorInformation(
                displayName: "[ECCentral]添加Area中的增删改查",
                description: "在Web项目的Area中生成相应的增删改查代码",
                author: "real罗阳铭",
                version: new Version(0, 1, 0, 0),
                id: "YMC_ECCentral_Scaffolding",
                icon: ToImageSource(Resources.Application),
                gestures: new[] { "YMC" },
                categories: new[] { "YMC", Categories.Common, Categories.Other }
            );
        }

        /// <summary>
        /// Helper method to convert Icon to Imagesource.
        /// </summary>
        /// <param name="icon">Icon</param>
        /// <returns>Imagesource</returns>
        public static ImageSource ToImageSource(Icon icon)
        {
            ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            return imageSource;
        }
    }

    public class KingsWebDtoDescription : CodeGeneratorFactory
    {
        public KingsWebDtoDescription()
            : base(CreateCodeGeneratorInformation())
        {
            
        }
        public override ICodeGenerator CreateInstance(CodeGenerationContext context)
        {
            return new DtoScaffolder(context, Information);
        }

        // We support CSharp WAPs targetting at least .Net Framework 4.5 or above.
        // We DON'T currently support VB
        public override bool IsSupported(CodeGenerationContext codeGenerationContext)
        {
            if (ProjectLanguage.CSharp.Equals(codeGenerationContext.ActiveProject.GetCodeLanguage()))
            {
                FrameworkName targetFramework = codeGenerationContext.ActiveProject.GetTargetFramework();
                return (targetFramework != null) &&
                        String.Equals(".NetFramework", targetFramework.Identifier, StringComparison.OrdinalIgnoreCase) &&
                        targetFramework.Version >= new Version(4, 5);
            }

            return false;
        }

        private static CodeGeneratorInformation CreateCodeGeneratorInformation()
        {
            return new CodeGeneratorInformation(
                displayName: "获取Dto文档",
                description: "前端人员获取Dto文档",
                author: "寒飞",
                version: new Version(0, 1, 0, 0),
                id: "YMC_ECCentral_Scaffolding",
                icon: ToImageSource(Resources.Application),
                gestures: new[] { "YMC" },
                categories: new[] { "YMC", Categories.Common, Categories.Other }
            );
        }

        /// <summary>
        /// Helper method to convert Icon to Imagesource.
        /// </summary>
        /// <param name="icon">Icon</param>
        /// <returns>Imagesource</returns>
        public static ImageSource ToImageSource(Icon icon)
        {
            ImageSource imageSource = Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            return imageSource;
        }
    }
}
