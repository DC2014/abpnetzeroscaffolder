﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YMC.ECCentral.Scaffolding.Models
{
    public enum euControlType
    {
        Text,
        Textarea,
        TextEditor,
        Hidden,
        DatePicker,
        DateTimePicker,
        DropdownList,
        Checkbox
    }
}
