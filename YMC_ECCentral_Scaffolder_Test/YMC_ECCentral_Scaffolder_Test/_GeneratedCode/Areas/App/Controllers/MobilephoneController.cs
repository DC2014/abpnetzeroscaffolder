﻿
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using YMC_ECCentral_Scaffolder_Test.App.Application;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Web.Mvc.Authorization;
using ShuiHuo.AbpZeroTemplate.Authorization;
using ShuiHuo.AbpZeroTemplate.MobilephoneApp;
using ShuiHuo.AbpZeroTemplate.MobilephoneApp.Dto;
using ShuiHuo.AbpZeroTemplate.Web.Areas.Mpa.Models.Mobilephone;
using ShuiHuo.AbpZeroTemplate.Web.Controllers;


namespace YMC_ECCentral_Scaffolder_Test.Web.Areas.Mpa.Controllers
{
    [AbpAuthorize(AppPermissions.Pages_Business_Mobilephone)]
    public class MobilephoneController : AbpZeroTemplateControllerBase
    {

        private readonly IMobilephoneAppService _mobilephoneAppService;

        public MobilephoneController(IMobilephoneAppService mobilephoneAppService)
        {
            _mobilephoneAppService = mobilephoneAppService;
           
        }
		
        // GET: Mpa/Mobilephone
        public ActionResult Index()
        {
            return View();
        }

		/// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Business_Mobilephone_Create)]
        public ActionResult CreateModal()
        {
            return PartialView("_CreateModal");
        }


       /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AbpMvcAuthorize(AppPermissions.Pages_Business_Mobilephone_Edit)]
        public ActionResult EditModal(int id)
        {
            MobilephoneOutput mobilephone = _mobilephoneAppService.GetMobilephoneForEdit(new EntityDto(id));
            MobilephoneViewModel mobilephoneViewModel = new MobilephoneViewModel(mobilephone);
            return PartialView("_EditModal", mobilephoneViewModel);
        }


    }
}