﻿

(function () {
    $(function () {

        var _$dataTable = $('#MobilephoneTable');
        var _appService = abp.services.app.mobilephone;
        //权限
        var _permissions = {
            create: abp.auth.hasPermission('Pages.Business.Mobilephone.Create'),
            edit: abp.auth.hasPermission('Pages.Business.Mobilephone.Edit'),
            'delete': abp.auth.hasPermission('Pages.Business.Mobilephone.Delete')
        };
        _$dataTable.jtable({
            title: app.localize('Mobilephone'),//标题
            paging: true,//启用分页
            sorting: true,//启用排序
            multiSorting: true,//启用多列排序
            actions: {
                listAction: {
                    method: _appService.getMobilephone//此处注意：方法名首字母必须小写
                }
            },
            fields: {
                id: {
                    key: true,
                    list: false
                },
                actions: {
                    title: app.localize('Actions'),//操作列
                    width: '15%',
                    sorting: false,
                    display: function (data) {
                        var $span = $('<span></span>');
                        if (_permissions.edit) {//判断是否有编辑权限
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Edit') + '"><i class="fa fa-edit"></i></button>')
                                    .appendTo($span)
                                    .click(function () {
                                        _editModal.open({ id: data.record.id });
                                    });
                        }
                        if (_permissions.delete) {//判断是否有删除权限
                            $('<button class="btn btn-default btn-xs" title="' + app.localize('Delete') + '"><i class="fa fa-trash-o"></i></button>')
                                    .appendTo($span)
                                    .click(function () {
                                        deleteMobilephone(data.record);
                                    });
                        }
                        return $span;
                    }
                },
					initPrice: {//此处注意，绑定字段首字母必须小写
                    title: app.localize('InitPrice'),
                    width: '20%'
                },
   	currentPrice: {//此处注意，绑定字段首字母必须小写
                    title: app.localize('CurrentPrice'),
                    width: '20%'
                },
               }
        });
        //获取列表
        function GetMobilephone(reload) {
            if (reload) {
                _$dataTable.jtable('reload');
            } else {
                _$dataTable.jtable('load', {
                    filter: $('#MobilephoneTableFilter').val()
                });
            }
        }
        //页面加载完执行
        GetMobilephone();



        //新增
        var _createModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Mobilephone/CreateModal',//加载视图
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Mobilephone/MobilephoneCreateModal.js',//加载对应js
            modalClass: 'CreateMobilephoneModal'
        });
        var _editModal = new app.ModalManager({
            viewUrl: abp.appPath + 'Mpa/Mobilephone/EditModal',
            scriptUrl: abp.appPath + 'Areas/Mpa/Views/Mobilephone/MobilephoneEditModal.js',
            modalClass: 'EditMobilephoneModal'
        });
        //添加点击事件
        $('#CreateNewItemButton').click(function () {
            _createModal.open();
        });

        //事件注册
        abp.event.on('app.createMobilephoneModalSaved', function () {
            GetMobilephone(true);
        });
        abp.event.on('app.editMobilephoneModalSaved', function () {
            GetMobilephone(true);
        });


        //删除分类
        function deleteMobilephone(mobilephone) {
            abp.message.confirm(
                app.localize('MobilephoneDeleteWarningMessage', mobilephone.name),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _appService.deleteMobilephone({
                            id: mobilephone.id
                        }).done(function () {
                            GetMobilephone();
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        //搜索点击事件
        $('#GetMobilephoneButton').click(function (e) {
            //取消事件的默认动作
            e.preventDefault();
            GetMobilephone();
        });

    });
})();