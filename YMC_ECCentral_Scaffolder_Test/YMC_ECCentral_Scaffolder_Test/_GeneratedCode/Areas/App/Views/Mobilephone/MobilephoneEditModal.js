﻿
var EditMobilephoneModal = (function ($) {
    app.modals.EditMobilephoneModal = function () {

        var _modalManager;
        var _appService = abp.services.app.mobilephone;
        var __$ModalForm = null;


        this.init = function (modalManager) {
            _modalManager = modalManager;

            __$ModalForm = _modalManager.getModal().find('form[name=MobilephoneForm]');
            __$ModalForm.validate();
        };

        this.save = function () {
            if (!__$ModalForm.valid()) {
                return;
            }

            var mobilephone = __$ModalForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _appService.updateMobilephone(
                mobilephone
            ).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.editMobilephoneModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);