﻿
(function ($) {

    app.modals.CreateMobilephoneModal = function () {
        var _appService = abp.services.app.mobilephone;
        var _$ModalForm = null;

        var _modalManager;
        this.init = function (modalManager) {
            _modalManager = modalManager;
            //取出Form表单
            _$ModalForm = _modalManager.getModal().find('form[name=MobilephoneForm]');
        };

        this.save = function () {
            //验证不通过返回
            if (!_$ModalForm.valid()) {
                return;
            }
            //序列化参数
            var mobilephone = _$ModalForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _appService.createMobilephone(
                mobilephone
            ).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createMobilephoneModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);