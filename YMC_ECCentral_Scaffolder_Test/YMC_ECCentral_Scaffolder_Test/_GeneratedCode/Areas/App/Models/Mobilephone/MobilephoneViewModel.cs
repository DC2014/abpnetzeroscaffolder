﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFramework;
using Abp.Extensions;
using Newtonsoft.Json;

namespace YMC_ECCentral_Scaffolder_Test..Web.Areas.Mpa.Models.Mobilephone
{
    [AutoMapFrom(typeof(MobilephoneOutput))]
    public class MobilephoneViewModel: MobilephoneOutput
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="output"></param>
        public MobilephoneViewModel(MobilephoneOutput output)
        {
            output.MapTo(this);
        }
    }
}
