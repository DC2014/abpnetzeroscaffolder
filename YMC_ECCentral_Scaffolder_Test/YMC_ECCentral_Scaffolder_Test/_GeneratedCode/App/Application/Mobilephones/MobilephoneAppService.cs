﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFramework;
using Abp.Extensions;
using Newtonsoft.Json;

namespace YMC_ECCentral_Scaffolder_Test.Mobilephones
{
    [AbpAuthorize(AppPermissions.Pages_Business_Mobilephone)]
    public class MobilephoneAppService : LogisticsAppServiceBase, IMobilephoneAppService
    {
        private readonly IRepository<Mobilephone,long> _mobilephoneRepository;

        public MobilephoneAppService(
            IRepository<Mobilephone,long> mobilephoneRepository
            )
        {
            _mobilephoneRepository = mobilephoneRepository;
        }

        #region 手机管理

        /// <summary>
        /// 根据查询条件获取手机分页列表
        /// </summary>
        public async Task<PagedResultOutput<MobilephoneOutput>> GetMobilephone(GetMobilephoneInput input)
        {
            var query = await _mobilephoneRepository.GetAll();
                //TODO:根据传入的参数添加过滤条件

				var MobilephoneCount=await query.CountAsync();

				var dtos=await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

				  var dtoList = dtos.MapTo<List<MobilephoneOutput>>();
				              return new PagedResultOutput<CarrierInfoQueryDto>(MobilephoneCount, dtoList);

         }

        /// <summary>
        /// 获取指定id的手机信息
        /// </summary>
        public async Task<MobilephoneOutput> GetMobilephone(long id)
        {
            var entity = await _mobilephoneRepository.GetAsync(id);
            return entity.MapTo<MobilephoneOutput>();
        }

		/// <summary>
        /// 通过主键获取实体
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
       public MobilephoneOutput GetMobilephoneForEdit(EntityDto input);
	   {
	        var entity = _mobilephoneRepository.Get(input.Id);
            return Mapper.Map<MobilephoneOutput>(entity);
	   }


        /// <summary>
        /// 新增或更改手机
        /// </summary>
        public async Task CreateOrUpdateMobilephone(CreateMobilephoneInput input)
        {
            if (input.Id ==null)
            {
                await CreateMobilephone(input);
            }
            else
            {
                await UpdateMobilephone(input);
            }
        }

        /// <summary>
        /// 新增手机
        /// </summary>
        [AbpAuthorize(AppPermissions.Pages_Business_Mobilephone_Create)]
        public virtual async Task<CreateMobilephoneInput> CreateMobilephone(CreateMobilephoneInput input)
        {
            //if (await _mobilephoneRepository.IsExistsMobilephoneByName(input.CategoryName))
            //{
            //    throw new UserFriendlyException(L("NameIsExists"));
            //}
            var entity = await _mobilephoneRepository.InsertAsync(input.MapTo<Mobilephone>());
            return entity.MapTo<CreateMobilephoneInput>();
        }

        /// <summary>
        /// 更新手机
        /// </summary>
        [AbpAuthorize(AppPermissions.Pages_Business_Mobilephone_Update)]
        public virtual async Task UpdateMobilephone(CreateMobilephoneInput input)
        {
            //if (await _mobilephoneRepository.IsExistsMobilephoneByName(input.CategoryName, input.Id))
            //{
            //    throw new UserFriendlyException(L("NameIsExists"));
            //}
            var entity = await _mobilephoneRepository.GetAsync(input.Id);
            await _mobilephoneRepository.UpdateAsync(input.MapTo(entity));
        }

        /// <summary>
        /// 删除手机
        /// </summary>
        [AbpAuthorize(AppPermissions.Pages_Business_Mobilephone_Delete)]
        public async Task DeleteMobilephone(EntityDto input)
        {
            //TODO:删除前的逻辑判断，是否允许删除
            await _mobilephoneRepository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// 批量删除手机
        /// </summary>
        [AbpAuthorize(AppPermissions.Pages_Business_Mobilephone_Delete)]
        public async Task BatchDeleteMobilephone(IEnumerable<long> input)
        {

		var longId = Convert.ToInt64(input);
            //TODO:批量删除前的逻辑判断，是否允许删除
            await _mobilephoneRepository.DeleteAsync(longId);
        }

        #endregion

    }
}
