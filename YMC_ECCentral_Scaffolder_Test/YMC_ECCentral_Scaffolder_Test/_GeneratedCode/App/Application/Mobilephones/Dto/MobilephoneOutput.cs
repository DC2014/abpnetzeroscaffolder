﻿


using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Extensions;

namespace YMC_ECCentral_Scaffolder_Test.Mobilephones.Dto
{
    [AutoMapFrom(typeof(Mobilephone))]
    public class MobilephoneOutput : EntityDto<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal InitPrice { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal CurrentPrice { get; set; }

    }
}
