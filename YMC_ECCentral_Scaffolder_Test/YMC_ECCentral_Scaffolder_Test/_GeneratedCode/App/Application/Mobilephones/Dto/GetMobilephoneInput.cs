﻿



















using System;
using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;
using YMC_ECCentral_Scaffolder_Test.App.Dto;

namespace YMC_ECCentral_Scaffolder_Test.Mobilephones.Dto
{
    public class GetMobilephoneInput : PagedAndSortedInputDto,IShouldNormalize
    {
        //DOTO:在这里增加查询参数
        public string Filter { get; set; }

        /// <summary>
        /// 规格化输入参数值
        /// </summary>
        public virtual void Normalize()
        {
            PageSize = Consts.MaxPageSize;  

            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CreationTime";
            }
        }

    }
}
