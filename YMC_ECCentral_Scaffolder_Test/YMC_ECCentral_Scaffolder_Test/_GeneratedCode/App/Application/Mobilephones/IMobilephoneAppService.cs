﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace YMC_ECCentral_Scaffolder_Test.Mobilephones
{
    public interface IMobilephoneAppService : IApplicationService
    {
        #region 手机管理

        /// <summary>
        /// 根据查询条件获取手机分页列表
        /// </summary>
        Task<PagedResultOutput<MobilephoneOutput>> GetMobilephone(GetMobilephoneInput input);

        /// <summary>
        /// 获取指定id的手机信息
        /// </summary>
        Task<MobilephoneOutput> GetMobilephone(long id);

		   /// <summary>
        /// 通过主键获取实体
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        MobilephoneOutput GetMobilephoneForEdit(EntityDto input);

        /// <summary>
        /// 新增或更改手机
        /// </summary>
        Task CreateOrUpdateMobilephone(CreateMobilephoneInput input);

        /// <summary>
        /// 新增手机
        /// </summary>
        Task<CreateMobilephoneInput> CreateMobilephone(CreateMobilephoneInput input);

        /// <summary>
        /// 更新手机
        /// </summary>
        Task UpdateMobilephone(CreateMobilephoneInput input);

        /// <summary>
        /// 删除手机
        /// </summary>
        Task DeleteMobilephone(EntityDto input);

        /// <summary>
        /// 批量删除手机
        /// </summary>
        Task BatchDeleteMobilephone(IEnumerable<long> input);

        #endregion

    }
}
