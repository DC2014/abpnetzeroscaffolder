﻿












using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Abp.EntityFramework;
using Abp.Domain.Repositories;
using Abp.Extensions;
using YMC.ECCentral.Core;
using YMC_ECCentral_Scaffolder_Test.App;

namespace YMC_ECCentral_Scaffolder_Test.EntityFramework.Repositories
{
    public class MobilephoneRepository : AbpZeroTemplateRepositoryBase<Mobilephone>, IMobilephoneRepository
    {
        public MobilephoneRepository(IDbContextProvider<AbpZeroTemplateDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        ///// <summary>
        ///// 判断唯一性
        ///// </summary>
        //public async Task<bool> IsExistsMobilephoneByName(string name, Guid? id = null)
        //{
        //    var query = GetAll().Where(m => m.Name == name);
        //    if (id.HasValue)
        //    {
        //        query = query.Where(m => m.Id != id.Value);
        //    }
        //    return await query.AnyAsync();
        //}
    }
}
