﻿
using System.Collections.Generic;
using Abp.Authorization;
using Abp.Localization;

namespace YMC_ECCentral_Scaffolder_Test.App.Domain
{
    public partial class AppPermissions
    {
        //TODO:★请将以下内容剪切到AppPermissions.cs

        /// <summary>
        /// 手机管理权限
        /// </summary>
        public const string Pages_Business_Mobilephone = "Pages.Business.Mobilephone";
        public const string Pages_Business_Mobilephone_Create = "Pages.Business.Mobilephone.Create";
        public const string Pages_Business_Mobilephone_Update = "Pages.Business.Mobilephone.Update";
        public const string Pages_Business_Mobilephone_Delete = "Pages.Business.Mobilephone.Delete";
    }
}

//TODO:★请将以下内容剪切到AppAuthorizationProvider.cs
/*
            //手机管理
            var mobilephone = business.CreateChildPermission(AppPermissions.Pages_Business_Mobilephone, L("Mobilephone"));
            mobilephone.CreateChildPermission(AppPermissions.Pages_Business_Mobilephone_Create, L("CreateMobilephone"));
            mobilephone.CreateChildPermission(AppPermissions.Pages_Business_Mobilephone_Update, L("UpdateMobilephone"));
            mobilephone.CreateChildPermission(AppPermissions.Pages_Business_Mobilephone_Delete, L("DeleteMobilephone"));
*/

//TODO:★请将以下内容剪切到AbpZeroTemplate-zh-CN.xml
/*
    <!-- 手机管理 -->
    <text name="Mobilephone" value="手机管理" />
    <text name="CreateMobilephone" value="新增手机" />
    <text name="UpdateMobilephone" value="更新手机" />
    <text name="DeleteMobilephone" value="删除手机" />
	<text name="MobilephoneDeleteWarningMessage" value="手机 {0} 将被删除." />
		<text name="Name" value="名称" />
   	<text name="NickName" value="昵称" />
   	<text name="Description" value="描述" />
   	<text name="InitPrice" value="原价" />
   	<text name="CurrentPrice" value="现价" />
   
*/


/*
将下列代码放在YMC_ECCentral_Scaffolder_Test.App.Web.Navigation.PageNames.App.Common中：
                public const string Mobilephone = "Mobilephone";

*/