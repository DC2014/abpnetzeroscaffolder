﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Extensions;

namespace YMC_ECCentral_Scaffolder_Test.IRepositories
{
    public interface IMobilephoneRepository : IRepository<Mobilephone>
    {
        //Task<bool> IsExistsMobilephoneByName(string name, Guid? id = null);
    }
}
