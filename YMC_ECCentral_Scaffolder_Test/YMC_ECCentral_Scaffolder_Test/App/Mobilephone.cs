﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YMC_ECCentral_Scaffolder_Test.App
{
    /// <summary>
    /// 手机管理
    /// </summary>
    public class Mobilephone 
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 原价
        /// </summary>
        public decimal InitPrice { get; set; }
        /// <summary>
        /// 现价
        /// </summary>
        public decimal CurrentPrice { get; set; }
    }
}
