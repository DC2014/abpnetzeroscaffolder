﻿

















using System.Collections.Generic;
using Abp.Authorization;
using Abp.Localization;

namespace YMC_ECCentral_Scaffolder_Test.Entities.Application
{
    public partial class EntitiesPermissions
    {
        //TODO:★请将以下内容剪切到EntitiesPermissions.cs

        /// <summary>
        /// 手机管理管理权限
        /// </summary>
        public const string Mobilephone = "Entities.Mobilephone";
        public const string Mobilephone_CreateMobilephone = "Entities.Mobilephone.CreateMobilephone";
        public const string Mobilephone_UpdateMobilephone = "Entities.Mobilephone.UpdateMobilephone";
        public const string Mobilephone_DeleteMobilephone = "Entities.Mobilephone.DeleteMobilephone";

    }
}

//TODO:★请将以下内容剪切到EntitiesAuthorizationProvider.cs
/*
            //手机管理管理
            var mobilephone = module.CreateChildPermission(EntitiesPermissions.Mobilephone, L("Mobilephone"));
            mobilephone.CreateChildPermission(EntitiesPermissions.Mobilephone_CreateMobilephone, L("CreateMobilephone"));
            mobilephone.CreateChildPermission(EntitiesPermissions.Mobilephone_UpdateMobilephone, L("UpdateMobilephone"));
            mobilephone.CreateChildPermission(EntitiesPermissions.Mobilephone_DeleteMobilephone, L("DeleteMobilephone"));

*/

//TODO:★请将以下内容剪切到Web站点Permissions.xml
/*
    <!-- 手机管理管理 -->
    <text name="Mobilephone" value="手机管理管理" />
    <text name="CreateMobilephone" value="新增手机管理" />
    <text name="UpdateMobilephone" value="更新手机管理" />
    <text name="DeleteMobilephone" value="删除手机管理" />

*/
