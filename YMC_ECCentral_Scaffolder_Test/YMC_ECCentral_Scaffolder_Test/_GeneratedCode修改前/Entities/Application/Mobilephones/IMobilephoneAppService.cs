﻿


















using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;

namespace YMC_ECCentral_Scaffolder_Test.Entities.Application
{
    public interface IMobilephoneAppService : IApplicationService
    {
        #region 手机管理管理

        /// <summary>
        /// 根据查询条件获取手机管理分页列表
        /// </summary>
        Task<PagedResultOutput<MobilephoneQueryDto>> GetMobilephoneQuery(GetMobilephoneQueryInput input);


        /// <summary>
        /// 获取指定id的手机管理信息
        /// </summary>
        Task<MobilephoneDto> GetMobilephone(long id);

        /// <summary>
        /// 新增或更改手机管理
        /// </summary>
        Task CreateOrUpdateMobilephone(MobilephoneDto input);

        /// <summary>
        /// 新增手机管理
        /// </summary>
        Task<MobilephoneDto> CreateMobilephone(MobilephoneDto input);

        /// <summary>
        /// 更新手机管理
        /// </summary>
        Task UpdateMobilephone(MobilephoneDto input);

        /// <summary>
        /// 删除手机管理
        /// </summary>
        Task DeleteMobilephone(IdInput input);


        /// <summary>
        /// 批量删除手机管理
        /// </summary>
        Task BatchDeleteMobilephone(IEnumerable<long> input);



        #endregion

    }
}
