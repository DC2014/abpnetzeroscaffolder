﻿

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFramework;
using Abp.Extensions;
using Newtonsoft.Json;
using YMC_ECCentral_Scaffolder_Test.Entities.Domain;
using YMC_ECCentral_Scaffolder_Test.Entities;

namespace YMC_ECCentral_Scaffolder_Test.Entities.Application
{
    [AbpAuthorize(EntitiesPermissions.Mobilephone)]
    public class MobilephoneAppService : LogisticsAppServiceBase, IMobilephoneAppService
    {
        private readonly IRepository<Mobilephone,long> _mobilephoneRepository;

        public MobilephoneAppService(
            IRepository<Mobilephone,long> mobilephoneRepository
            )
        {
            _mobilephoneRepository = mobilephoneRepository;
        }

        #region 手机管理管理

        /// <summary>
        /// 根据查询条件获取手机管理分页列表
        /// </summary>
        public async Task<PagedResultOutput<MobilephoneQueryDto>> GetMobilephoneQuery(GetMobilephoneQueryInput input)
        {
            var query = await _mobilephoneRepository.GetAll();
                //TODO:根据传入的参数添加过滤条件

				var MobilephoneCount=await query.CountAsync();

				var dtos=await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

				  var dtoList = dtos.MapTo<List<MobilephoneQueryDto>>();
				              return new PagedResultOutput<CarrierInfoQueryDto>(MobilephoneCount, dtoList);

         }

        /// <summary>
        /// 获取指定id的手机管理信息
        /// </summary>
        public async Task<MobilephoneDto> GetMobilephone(long id)
        {
            var entity = await _mobilephoneRepository.GetAsync(id);
            return entity.MapTo<MobilephoneDto>();
        }

        /// <summary>
        /// 新增或更改手机管理
        /// </summary>
        public async Task CreateOrUpdateMobilephone(MobilephoneDto input)
        {
            if (input.Id ==null)
            {
                await CreateMobilephone(input);
            }
            else
            {
                await UpdateMobilephone(input);
            }
        }

        /// <summary>
        /// 新增手机管理
        /// </summary>
        [AbpAuthorize(EntitiesPermissions.Mobilephone_CreateMobilephone)]
        public virtual async Task<MobilephoneDto> CreateMobilephone(MobilephoneDto input)
        {
            //if (await _mobilephoneRepository.IsExistsMobilephoneByName(input.CategoryName))
            //{
            //    throw new UserFriendlyException(L("NameIsExists"));
            //}
            var entity = await _mobilephoneRepository.InsertAsync(input.MapTo<Mobilephone>());
            return entity.MapTo<MobilephoneDto>();
        }

        /// <summary>
        /// 更新手机管理
        /// </summary>
        [AbpAuthorize(EntitiesPermissions.Mobilephone_UpdateMobilephone)]
        public virtual async Task UpdateMobilephone(MobilephoneDto input)
        {
            //if (await _mobilephoneRepository.IsExistsMobilephoneByName(input.CategoryName, input.Id))
            //{
            //    throw new UserFriendlyException(L("NameIsExists"));
            //}
            var entity = await _mobilephoneRepository.GetAsync(input.Id);
            await _mobilephoneRepository.UpdateAsync(input.MapTo(entity));
        }

        /// <summary>
        /// 删除手机管理
        /// </summary>
        [AbpAuthorize(EntitiesPermissions.Mobilephone_DeleteMobilephone)]
        public async Task DeleteMobilephone(IdInput input)
        {
            //TODO:删除前的逻辑判断，是否允许删除
            await _mobilephoneRepository.DeleteAsync(input.Id);
        }

        /// <summary>
        /// 批量删除手机管理
        /// </summary>
        [AbpAuthorize(EntitiesPermissions.Mobilephone_DeleteMobilephone)]
        public async Task BatchDeleteMobilephone(IEnumerable<long> input)
        {

		var longId = Convert.ToInt64(input);
            //TODO:批量删除前的逻辑判断，是否允许删除
            await _mobilephoneRepository.DeleteAsync(longId);
        }

        #endregion

    }
}
