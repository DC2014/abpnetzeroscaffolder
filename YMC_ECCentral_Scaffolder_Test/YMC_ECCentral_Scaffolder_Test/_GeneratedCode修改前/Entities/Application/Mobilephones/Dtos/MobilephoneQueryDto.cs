﻿

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Extensions;
using YMC_ECCentral_Scaffolder_Test.Entities;
using YMC_ECCentral_Scaffolder_Test.Entities.Domain;

namespace YMC_ECCentral_Scaffolder_Test.Entities.Application
{
    [AutoMapFrom(typeof(Mobilephone))]
    public class MobilephoneQueryDto : EntityDto<long>
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        public decimal InitPrice { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        public decimal CurrentPrice { get; set; }

    }
}
