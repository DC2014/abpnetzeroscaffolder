﻿

using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using Abp.Extensions;
using YMC_ECCentral_Scaffolder_Test.Entities;
using YMC_ECCentral_Scaffolder_Test.Entities.Domain;

namespace YMC_ECCentral_Scaffolder_Test.Entities.Application
{
    /// <summary>
    /// 手机管理
    /// </summary>
    [AutoMap(typeof(Mobilephone))]
    public class MobilephoneDto : EntityDto<long>, IValidate
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string NickName { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [Required]
        [MaxLength(255)]
        public string Description { get; set; }

        /// <summary>
        /// 原价
        /// </summary>
        [Required]
        public decimal InitPrice { get; set; }

        /// <summary>
        /// 现价
        /// </summary>
        [Required]
        public decimal CurrentPrice { get; set; }

    }
}
